package com.company;

class AtBashCipher implements Cipher {
    @Override
    public String decode(String message) {
        StringBuilder decoded = new StringBuilder();
        for(char c : message.toLowerCase().toCharArray()){
            if(Character.isLetter(c)){
                int newChar = ('z' - c) + 'a';
                decoded.append((char) newChar);
            }
            else{
                decoded.append(c);
            }
        }
        return decoded.toString();
    }

    @Override
    public String encode(String message) {
        return decode(message);

    }
}